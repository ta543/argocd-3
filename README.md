# 🚀 Real-Time Data Analytics System

## 📝 Project Overview

This repository houses a cutting-edge real-time data analytics system, leveraging a microservices architecture to ingest, process, and display data dynamically. The system aims to offer real-time actionable insights via an engaging interactive dashboard.

## 🏗 Architecture

Our system comprises multiple microservices each dedicated to a core function:

- **Data Ingestion Service**: Ingests data from diverse sources.
- **Data Processing Service**: Derives insights from the ingested data.
- **Data Storage Service**: Archives processed data using a PostgreSQL database.
- **Frontend Service**: Visualizes data on a web-based dashboard.

## 🛠 Technologies Used

- **Kubernetes**: Orchestrates the containerized services.
- **Docker**: Containers for each service.
- **ArgoCD**: Manages continuous deployment within the Kubernetes ecosystem.
- **PostgreSQL**: Database for storing insightful data.
- **React**: Creates the interactive dashboard.
- **Flask/Express.js**: Powers the backend for the processing and ingestion services.

## 🗂 Directory Structure

```
real-time-data-analytics/
│
├── docs/ # Documentation files
│ ├── api/ # API documentation
│ ├── architecture.md # Architecture overview
│ └── setup.md # Setup instructions
│
├── infra/ # Infrastructure as code
│ ├── kubernetes/ # Kubernetes manifests
│ └── terraform/ # Terraform configuration files
│
└── services/ # Microservices
├── data-ingestion/
│ ├── src/
│ ├── Dockerfile
│ └── k8s/
├── data-processing/
│ ├── src/
│ ├── Dockerfile
│ └── k8s/
├── data-storage/
│ ├── src/
│ ├── Dockerfile
│ └── k8s/
└── frontend/
├── src/
├── Dockerfile
└── k8s/
```

## 📦 Prerequisites

To deploy this project, ensure you have:

- A Kubernetes cluster (e.g., minikube, EKS, GKE)
- `kubectl` configured to communicate with your cluster
- ArgoCD installed and configured in your cluster
- Terraform installed

## 🚀 Installation

Execute the following steps to prepare your environment:

1. **Clone the repository**:
    ```bash
    git clone https://gitlab.com/ta543/argocd-3.git
    cd argocd-3
    cd real-time-data-analytics
    ```

2. **Apply the Kubernetes configurations**:
    ```bash
    kubectl apply -f infra/kubernetes/
    ```

3. **Deploy using Terraform**:
    ```bash
    cd infra/terraform/
    terraform init
    terraform apply
    ```

4. **Access the ArgoCD UI**:
    - Enable access via port-forwarding:
    ```bash
    kubectl port-forward svc/argocd-server -n argocd 8080:443
    ```

## 📊 Usage

- **Data Ingestion**: Interface with the ingestion service via its endpoint.
- **Dashboard**: Navigate to the frontend service via its external IP to interact with the dashboard.

## License

This project is available under the [MIT License](https://choosealicense.com/licenses/mit/).

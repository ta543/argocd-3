document.addEventListener('DOMContentLoaded', function() {
    const display = document.getElementById('data-display');
    fetch('/api/data')
        .then(response => response.json())
        .then(data => {
            display.textContent = JSON.stringify(data, null, 2);
        })
        .catch(error => console.error('Error fetching data:', error));
});

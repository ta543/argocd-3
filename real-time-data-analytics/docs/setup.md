# Setup Instructions for Real-Time Data Analytics System

## Requirements
- Kubernetes cluster
- ArgoCD installed on the cluster
- Docker installed on your local machine
- Access to a Git repository

## Installation Steps
1. **Clone the repository:**
   ```bash
   git clone https://gitlab.com/ta543/argocd-3.git
   cd real-time-data-analytics
   ```

2. **Deploy ArgoCD Applications:**
   ```bash
   kubectl apply -f argocd/applications/dev/
   kubectl apply -f argocd/applications/staging/
   kubectl apply -f argocd/applications/prod/
   ```

3. **Initialize the Database:**
   ```bash
   kubectl apply -f services/data-storage/k8s/init-db.yaml
   ```

## Configuration
Detail the configuration steps necessary to tailor the application for your environment.

## Verification
Describe how to verify that the installation was successful.
